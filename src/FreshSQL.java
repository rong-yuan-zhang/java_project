import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.SQLException;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import java.sql.Statement;

public class FreshSQL {
    static String url = "jdbc:mysql://localhost:3306/bags"; // 本地student数据库地址
    static String username = "BagRoot";// MySQL数据库登录用户名
    static String password = "123456";// MySQL数据库登录密码

    // PreparedStatement preSql;
    static int num[] = new int[2];

    public FreshSQL() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");// 加载MySQL数据库驱动
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("未能成功加载数据库驱动程序！");
        }
    }

    public void FreshOperate() {
        try {
            Connection con2 = DriverManager.getConnection(url, username, password);

            Statement statement = con2.createStatement();
            String sql = "select * from bag;";
            ResultSet resultSet = statement.executeQuery(sql);

            int i = 0;
            while (resultSet.next()) {
                num[i] = resultSet.getInt("ObjNum");
                i++;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "刷新失败", "恭喜",
                    JOptionPane.WARNING_MESSAGE);
        }
    }

}

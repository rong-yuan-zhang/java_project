import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionListener;

public class ComboBoxListener implements ItemListener, ActionListener {
    MyFrame main;
    String object;

    public void itemStateChanged(ItemEvent e) {
        this.object = main.comboBox.getSelectedItem().toString();
        main.putListener.setObjct(object);
        main.takeListener.setObjct(object);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.object = main.comboBox.getSelectedItem().toString();
        main.putListener.setObjct(object);
        main.takeListener.setObjct(object);
    }
}

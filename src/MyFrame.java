import javax.swing.*;
import java.awt.event.*;
// import java.awt.*;
// //import java.awt.Image;
// import java.awt.Dimension;
// import java.awt.Graphics;
// import java.awt.Graphics2D;
// import java.awt.Image;
// import java.awt.Panel;
// import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

class MyFrame extends JFrame {
    static JComboBox<String> comboBox;
    static PutListener putListener = new PutListener();
    static TakeListener takeListener = new TakeListener();
    // private Image image;
    static JLabel ob1Num;
    static JLabel ob2Num;
    static int num[] = new int[2];

    public MyFrame() {
        this.setTitle("背包");
        this.setBounds(300, 300, 700, 500);

        // JPanel p = new JPanel();
        // p.setLayout(null);

        JButton put = new JButton("存放");
        put.setBounds(55, 400, 100, 20);
        put.addActionListener(putListener);

        JButton take = new JButton("取出");
        take.setBounds(165, 400, 100, 20);
        take.addActionListener(takeListener);

        JButton fresh = new JButton("刷新");
        fresh.setBounds(10, 10, 100, 30);

        fresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = e.getActionCommand();

                FreshSQL freshSQL = new FreshSQL();
                freshSQL.FreshOperate();
                num[0] = freshSQL.num[0];
                num[1] = freshSQL.num[1];
                System.out.println(str + "\t" + num[0] + "\t" + num[1]);
                ob1Num.setText("  " + num[0] + "");
                ob2Num.setText("  " + num[1] + "");
            }
        });

        this.add(put);
        this.add(take);
        this.add(fresh);

        comboBox = new JComboBox<String>();
        comboBox.addItem("apple");
        comboBox.addItem("herb");
        comboBox.setBounds(55, 360, 210, 20);
        ComboBoxListener comboBoxListener = new ComboBoxListener();
        comboBox.addItemListener(comboBoxListener);
        comboBox.addActionListener(comboBoxListener);
        this.add(comboBox);

        // this.add(bag);
        ImageIcon ob1 = new ImageIcon("src\\image\\apple.png");
        ob1.setImage(ob1.getImage().getScaledInstance(50, 50, 0));
        JLabel ob1Label = new JLabel(ob1);
        ob1Label.setBounds(352, 85, 60, 60);

        ImageIcon ob2 = new ImageIcon("src\\image\\herb.png");
        ob2.setImage(ob2.getImage().getScaledInstance(50, 50, 0));
        JLabel ob2Label = new JLabel(ob2);
        ob2Label.setBounds(415, 85, 60, 60);

        // ImageIcon ob3 = new ImageIcon("src\\image\\box.png");
        // ob2.setImage(ob3.getImage().getScaledInstance(30, 30, 0));
        // JLabel ob3Label = new JLabel(ob3);
        // ob3Label.setBounds(350, 200, 30, 30);
        ImageIcon button = new ImageIcon("src\\image\\button.png");
        button.setImage(button.getImage().getScaledInstance(30, 20, 0));
        // ImageIcon button2 = new ImageIcon("src\\image\\bt2.png");
        ImageIcon bg = new ImageIcon("src\\image\\bg.png");
        bg.setImage(bg.getImage().getScaledInstance(700, 500, 0));
        JLabel bgLabel = new JLabel(bg);
        bgLabel.setBounds(0, 0, 700, 500);

        // ImageIcon NI = new ImageIcon("src\\image\\apple.png");
        // NI.setImage(NI.getImage().getScaledInstance(30, 30, 0));
        ob1Num = new JLabel();
        ob1Num.setBounds(388, 125, 20, 20);
        ob1Num.setOpaque(true);
        // ob1Num.setText(freshListener.num[0] + "");
        ob1Num.setBackground(Color.black);
        ob1Num.setForeground(Color.white);

        ob2Num = new JLabel();
        ob2Num.setBounds(455, 125, 20, 20);
        ob2Num.setOpaque(true);
        // ob1Num.setText(freshListener.num[0] + "");
        ob2Num.setBackground(Color.black);
        ob2Num.setForeground(Color.white);

        JLabel N = new JLabel();
        take.setIcon(button);
        put.setIcon(button);
        fresh.setIcon(button);
        // 先添加的在上面
        this.add(ob1Num, null);
        this.add(ob2Num, null);
        this.add(ob1Label);
        this.add(ob2Label);
        // this.add(ob3Label, null);
        this.add(bgLabel);
        // JLabel bgLabel2 = new JLabel(bg);
        // bgLabel2.setBounds(0, 0, 700, 500);
        // this.add(bgLabel2);
        this.add(N);

        setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    // @Override
    // public void paint(Graphics g) {
    // Graphics2D g2d = (Graphics2D) g;
    // g2d.drawImage(image, 0, 0, null);
    // }
}
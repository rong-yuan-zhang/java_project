import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class TakeSQL {
    public String url = "jdbc:mysql://localhost:3306/bags"; // 本地student数据库地址
    private String username = "BagRoot";// MySQL数据库登录用户名
    private String password = "123456";// MySQL数据库登录密码
    public Connection connect;
    PreparedStatement preSql;

    public TakeSQL() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");// 加载MySQL数据库驱动
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("未能成功加载数据库驱动程序！");
        }
        try {
            this.connect = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("未能成功连接数据库！");
        }
    }

    public void writeTake(String fruit) {
        String sqlPut = "update bag set ObjNum=ObjNum-1 where ObjName=?";
        int ok = 0;
        try {
            preSql = this.connect.prepareStatement(sqlPut);
            preSql.setString(1, fruit);
            ok = preSql.executeUpdate();
            this.connect.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "取出失败", "警告",
                    JOptionPane.WARNING_MESSAGE);
        }
        if (ok != 0) {
            JOptionPane.showMessageDialog(null, "取出成功", "恭喜",
                    JOptionPane.WARNING_MESSAGE);
        }
    }
}
